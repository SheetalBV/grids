@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'tables'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header">
                    
                        <div class="row">
                            <div class="col-md-10">
                            <h4 class="card-title">Photo Gallery </h4> 
                            </div>

                            <div class="col-md-2">
                                
                                    <button class="btn btn-outline-primary btn-round" onclick="location.href='photo_gallery_add'">
                                        <i class="fa fa-plus"></i> Photo Gallery
                                      </button>
                            </div>
                            
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table"  id="datatable">
                                <thead class=" text-primary">                                   
                                    <th>Sr.No.</th>
                                    <th>Image</th>                                    
                                    <th>Title</th>                                    
                                    <th>Description</th>
                                    <th>Created On</th>                                   
                                    <th>Action</th>                                   
                                </thead>
                                <tbody>
                                    @foreach($photo_gallery as $key => $pg)
                                    <tr>
                                        <td>{{($key+1)}}</td>  
                                        <td><a href='images/{{$pg->image}}' target="_blank">View</a></td>                                                                              
                                        <td>{{$pg->title}}</td>                                        
                                        <td>{{$pg->description}}</td>
                                        <td>{{$pg->createdon}}</td>
                                        <td>
                                            <a href='photo_gallery_edit?id={{$pg->ID}}'><i class="fa fa-pencil-square-o"></i></a>
                                            <a href='photo_gallery_delete?id={{$pg->ID}}'><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>  
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!-- <script>
    $(document).ready(function () {
        $('#escalation').DataTable();
    });
</script> -->