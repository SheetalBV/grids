@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Oral Pathology  </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Oral Pathology </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">This department is involved with the training of Undergraduate students and teaches Dental Anatomy, Oral Physiology, Oral Embryology & Oral Histology in the first year of BDS. Practical training includes carving and identification of teeth, age estimation from plaster casts and interpretation of Histological slides. This helps them gain sound clinical judgment, adopt a rational treatment procedure and better understand the basis of Oral Pathology, which will be taught in second and third years of BDS.</p>
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>Prosthetics is the main subject of the Dental College, we have major three departments:</p>
  <ul>
    <li>50 Monocular Light Microscopes for undergraduate students.</li>
    <li>Binocular & Research Microscopes.</li>
    <li>Study models & step by step procedure for teaching of tooth carving.</li>
    <li>Museum for Specimens.</li>
    <li>Dental chair with airoter & Micro motor hand pieces.</li>
    <li>Departmental Library.</li>
    <li>Well Equipped histopathology Laboratory.</li>
  </ul>
  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/OralPathology1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/OralPathology2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>

 </div>
</section>

@endsection