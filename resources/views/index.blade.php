@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="heroarea clearfix">
 <div class="slider hero-slide">
    @foreach($sliders as $slider)
    <div style="background:url(images/{{$slider->image}}) left top no-repeat; background-size:cover;">
            <h1><span>{{$slider->message}}</span></h1>
    </div>
    @endforeach
    <!-- <div style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
      <h1><span>Developing the next generation of dental care leaders.</span> </h1>
    </div>
 
  <div style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
    <h1><span>Laying the foundation for a rich, fulfilling career in oral hygiene.</span></h1>
  </div> -->
        
</div>
</section>

<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container">
   <h2>Welcome to</h2>
   <h3>GRIDS</h3>
   <p>Shining the light in dental science</p>
   <p>We are one of the leading institutions in Gujarat for dental science, bridging academic excellence with clinical demand for highly skilled dentists and oral care professionals. Established in 2010 and recognized by Gujarat University, we now have over 2500 dentists pursuing their career in clinics across India. </p>
   <p><a href="courses.html"><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></a></p>
 </div>
</section>


<!-- Gallery Home Start -->
<section class="homegallery clearfix">

 <div class="container-fluid">
  <div class="row">
   <div class="col-sm-8"><img src="images/photo-gallery-home.jpg" class="img-fluid home-gallery" alt="Independence"></div>
   <div class="col-sm-4">
    <h2><i></i> Independence day-<span>15.08.2018</span></h2>
    <p>Bholaram Education Society celebrated Independence Day at Goenka Campus, Gandhinagar on 15th August 2018, with great enthusiasm and respect. The ceremony started at 8:30 AM in the... </p>
    <p><a href="/news-and-event">Read More</a></p>
   </div>
  </div>
 </div>

 <div class="container-fluid">
  <div class="row">
   <div class="col-sm-8 m-hide"><img src="images/photo-gallery-home.jpg" class="img-fluid home-gallery1" alt="Independence"></div>
  <div class="col-sm-4">
    <h2><i></i> Depression and Suicide Prevention Program<span>-March 2018</span></h2>
    <p><a href="/news-and-event">Read More</a></p>
   </div>
   <div class="col-sm-8 d-hide"><img src="images/depression.jpg" class="img-fluid home-gallery1" alt="Independence"></div>
   
  </div>
 </div>

 <div class="container-fluid nopad">
  <div class="row">
   <div class="col-sm-8"><img src="images/Root_2018.jpg" class="img-fluid home-gallery2" alt="Independence"></div>
   <div class="col-sm-4">
    <h2><i></i> ROOTS<span>-2018</span></h2>
    <p><a href="/news-and-event">Read More</a></p>
   </div>
  </div>
 </div>

</section>


<!-- Chairman's Desk Start -->
<section class="ChairmanDesk clearfix">
 <div class="container">
   <div class="row">
    
    <div class="col-sm-6">
    <center>
      <img src="images/Chairman.png" class="img-fluid" alt="Chairman's ">
    </center>
    </div>
    <div class="col-sm-6">
      <h2>Chairman's Desk</h2>
      <p>Today dentistry is successful in occupying a distinct sphere of its own in the vast expanse of medical world. Having gained recognition as a specialized vista, this field of medical study has come up with immense scopes. Harping on the prospect and personal interest in this particular medical principle, umpteen numbers of students are taking up dentistry in their bachelors’ degree.
...</p>
<p>In this rapidly evolving age of dental education it remains essential for doctors to understand not only the significance of dental care and importance of oral hygiene, but also keep abreast with research and latest developments.</p>
      <p><a href="about.html">Read More</a></p>
    </div>
    
   </div>
 </div>
</section>


<!-- News & Event Start -->
<section class="homenews clearfix">
 <div class="container">
  <h2>Latest</h2>
  <h3>News & Events</h3>
   
   <div class="row">
    <div class="col-sm-4">
      <div class="newsblog clearfix">
       <div class="blog-header" style=" background-image:url(images/OralPathology2.jpg);"></div> 
       <h4><a href="#">Conservative Dentistry and Endodontic  CDE Programme on 15th May,2019.</a></h4>
       <p><a href="news-and-event.html">View More  <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
      </div>
    </div>
    
     <div class="col-sm-4">
      <div class="newsblog clearfix">
       <div class="blog-header" style=" background-image:url(images/02.jpg);"></div> 
       <h4><a href="#">UDAN FESTIVAL AT GRIDS</a></h4>
        <p>We Organized Udaan as a Pre Uttarayan Celebration on 06/01/2018</p>
       <p><a href="news-and-event.html">View More  <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
      </div>
    </div>
    
    <div class="col-sm-4">
      <div class="newsblog clearfix">
       <div class="blog-header" style=" background-image:url(images/03.jpg);"></div> 
       <h4><a href="#">Blood Donation Camp in Rajbhavan, Gandhinagar</a></h4>
        <p>Birthday of Our Honorable Governor</p>
       <p><a href="news-and-event.html">View More  <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
      </div>
    </div>
   
   </div>
  
 </div>
</section>
@endsection