@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'tables'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header">
                    
                        <div class="row">
                            <div class="col-md-10">
                            <h4 class="card-title">Slider </h4> 
                            </div>

                            <div class="col-md-2">
                                
                                    <button class="btn btn-outline-primary btn-round" onclick="location.href='slider_add'">
                                        <i class="fa fa-plus"></i> Slider
                                      </button>
                            </div>
                            
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table"  id="datatable">
                                <thead class=" text-primary">                                   
                                    <th>Sr.No.</th>
                                    <th>Image</th>
                                    <th>Message</th>
                                    <th>Created On</th>                                   
                                    <th>Action</th>                                   
                                </thead>
                                <tbody>
                                    @foreach($sliders as $key => $slider)
                                    <tr>
                                        <td>{{($key+1)}}</td>                                        
                                        <td><a href='images/{{$slider->image}}' target="_blank">View</a></td>
                                        <td>{{$slider->message}}</td>
                                        <td>{{$slider->createdon}}</td>
                                        <td>
                                            <a href='slider_edit?id={{$slider->ID}}'><i class="fa fa-pencil-square-o"></i></a>
                                            <a href='slider_delete?id={{$slider->ID}}'><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>  
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!-- <script>
    $(document).ready(function () {
        $('#escalation').DataTable();
    });
</script> -->