@extends('layouts.app', [
'class' => '',
'elementActive' => 'tables'
])

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-plain">
                <h4 class="card-title">Add Slider Image</h4>
                <form method="post" action="{{ URL('slider_store') }}" enctype='multipart/form-data'>
                @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <h6>Image<sup class="star">*</sup></h6>
                            <input type="file" class="form-control" accept="image/*" id="image" name="image" placeholder="" required>
                        </div>                        
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <h6>Message<sup class="star">*</sup></h6>                            
                            <textarea class="form-control" id="message" name="message" required></textarea>
                        </div>                        
                    </div>
                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function () {
        $("#image").change(function() {

            debugger;
            var val = $(this).val();
            switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
                case 'jpeg': case 'jpg': case 'png':
                    //alert("an image");
                    break;
                default:
                    $(this).val('');
                    // error message here
                    alert("File is invalid");
                    break;
            }
        });

        $("#video").change(function() {

            debugger;
            var val = $(this).val();
            switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
                case 'mp4': case 'mov': case 'mkv':
                    //alert("an image");
                    break;
                default:
                    $(this).val('');
                    // error message here
                    alert("File is invalid");
                    break;
            }
        });
    });
</script>