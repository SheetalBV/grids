@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/gallery-banner.png) left top no-repeat; background-size:cover;">
  <h1>Photo Gallery </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li>Photo Gallery</li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="gallerywrap clearfix">
<div class="container">
 <div class="row">
   
   @if(count($photo_gallery) > 0)
      @foreach($photo_gallery as $pg)
      <div class="col-lg-6 col-12">
        <div class="blogbox clearfix">
          <div class="gallery-header" style="background-image:url(images/{{ $pg->image }})">
          <!-- <a href="#">View Gallery</a> -->
        </div>
          <h2><i></i>{{ $pg->title }}</h2>
          <p>{{ $pg->description }} </p>
        </div>
      </div>
      @endforeach
   @else
    <h3 style="text-align:center;">No Data Found</h3>   
   @endif
   <!-- <div class="col-lg-6 col-12">
     <div class="blogbox clearfix">
       <div class="gallery-header" style="background-image:url(images/gallery1.png)">
      <a href="#">View Gallery</a>
     </div>
       <h2><i></i>Independence day<span>-15.08.2018</span></h2>
       <p>Bholaram Education Society celebrated Independence Day at Goenka Campus, Gandhinagar on 15th August 2018, with great enthusiasm and respect. The ceremony started at 8:30 AM in the... </p>
     </div>
   </div>
   
   <div class="col-lg-6 col-12">
     <div class="blogbox clearfix">
       <div class="gallery-header" style="background-image:url(images/gallery2.png)">
      <a href="#">View Gallery</a>
     </div>
       <h2><i></i>Depression and Suicide Prevention Program<span>-March 2018</span></h2>
     </div>
   </div> 
   
   <div class="col-lg-6 col-12">
     <div class="blogbox clearfix">
       <div class="gallery-header" style="background-image:url(images/gallery3.png)">
      <a href="#">View Gallery</a>
     </div>
       <h2><i></i>March <span>-2018</span></h2>
     </div>
   </div>
   
   <div class="col-lg-6 col-12">
     <div class="blogbox clearfix">
       <div class="gallery-header" style="background-image:url(images/gallery4.png)">
      <a href="#">View Gallery</a>
     </div>
       <h2><i></i>Holi Celebration<span>-2018</span></h2>
     </div>
   </div> 
   
   <div class="col-lg-6 col-12">
     <div class="blogbox clearfix">
       <div class="gallery-header" style="background-image:url(images/gallery5.png)">
      <a href="#">View Gallery</a>
     </div>
       <h2><i></i>Sapavada Mega <span>-Camp</span></h2>
     </div>
   </div>
   
   <div class="col-lg-6 col-12">
     <div class="blogbox clearfix">
       <div class="gallery-header" style="background-image:url(images/gallery6.png)">
      <a href="#">View Gallery</a>
     </div>
       <h2><i></i>Republic Day<span>-2018</span></h2>
     </div>
   </div>  -->
   
 </div>
</div>
</section> 
@endsection