@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Community Dentistry   </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Community Dentistry  </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">This initiative places importance on interacting with the community to promote awareness about dental health, conducting surveys for collecting epidemiological data and preventing dental disease. Undergraduate students are taught to use various periodontal instruments, which is useful in everyday dental practice. Interns are trained in some specialized work as well, to help them integrate periodontics with their dental practice.</p>

   <p><center><a href="/courses"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<!-- <section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>The department has dental chairs and all the latest equipments required for the in depth study of radiology. Also we have radiology department is in the same department having Digital OPG, Intraoral X-Ray with RVG facility and Dark Room with automatic X-Ray developer unit.</p>
  
  <p><center>
    <img src="images/facilitate.png" class="img-fluid" alt="facilitate ">
  </center></p>
 </div>
</section> -->

@endsection