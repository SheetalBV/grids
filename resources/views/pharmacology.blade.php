@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Pharmacology  </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Pharmacology </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">Pharmacology deals with the study of drugs. As a Dental Technician, one is required to assist in procuring, labelling, and storing drugs. The department imparts knowledge about choice and use of drugs, to help achieve improved therapeutics.</p>
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>We have a fully equipped lab for pharmacology with all latest & required instruments.</p>
 
  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/Pharmacology1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/Pharmacology2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
 </div>
</section>
@endsection