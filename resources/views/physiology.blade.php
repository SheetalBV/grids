@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Physiology </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Physiology</li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container">
   <p>The human body has various processes such as rest, reproduction, extermination of waste, etc. The study of physiology enables students to apply modern science and rationale in understanding the differences between health and ill-health.</p>
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>We have one common lab for physiology & Biochemistry department. This is divided into two sections with a comfortable HOD’s room and spacious staff room, a preparation room, a clinical room for all physiological students, sufficient laboratory tables which are designed with sinks, racks for storage of chemical bottles and gas burners. Besides these this department also has at its disposal a preparation room which is used for preparing different solution for different practicals.</p>
  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/Physiology-1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/Physiology2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
 </div>
</section>
@endsection