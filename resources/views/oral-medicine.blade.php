@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Oral Medicine    </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Oral Medicine   </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">Oral Medicine includes the diagnosis and medical management of diseases specific to the orofacial tissues and of oral manifestations of systemic diseases. It further includes the management of behavioural disorders and the oral and dental treatment of medically compromised patients.</p>

   <p><center><a href="/courses"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<!-- <section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>The department has dental chairs and all the latest equipments required for the in depth study of radiology. Also we have radiology department is in the same department having Digital OPG, Intraoral X-Ray with RVG facility and Dark Room with automatic X-Ray developer unit.</p>
  
  <p><center>
    <img src="images/facilitate.png" class="img-fluid" alt="facilitate ">
  </center></p>
 </div>
</section> -->
@endsection