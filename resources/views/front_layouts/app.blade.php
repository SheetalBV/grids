<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Grids</title>
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png"  />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="css/slick.css" rel="stylesheet" type="text/css"/>
<link href="css/styles.css" rel="stylesheet" type="text/css"/>
<link href="css/responsive.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<div class="mainpage clearfix">

<!-- Top Menu Start -->
<section class="TopMenu clearfix">
 <div class="container">
 
      <nav class="navbar navbar-expand-md">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto topmenuleft">
            <li class="nav-item active">
              <a class="nav-link" href="/about">Grids@glance</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/aboutBES">Infrastructure</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/courses">Dentistry Courses</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/enquiry">Admission Form</a>
            </li>
          </ul>
          <ul class="navbar-nav topmenuright">
            <!-- <li class="nav-item">
              <a class="nav-link" href="#">Careers</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link" href="/contact">Contact Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/enquiry">Enquiry Form</a>
            </li>
            <li class="nav-item">
              <!-- <a class="nav-link" href="#">Sitemap</a> -->
            </li>
          </ul>
          
           
        </div>
      </nav>
 </div>
</section>

<!-- Header Start -->
<header>
 <div class="container">
  <nav class="navbar navbar-expand-md">
  <!-- Brand -->
  <a class="navbar-brand" href="/"><img src="images/logo.png" alt="Grids"></a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"><img src="images/list.svg" alt="List"></span>
  </button>

  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav mr-auto topmenuleft">
            <li class="nav-item active">
              <a class="nav-link" href="/about">Grids@glance</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/aboutBES">Infrastructure</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/courses">Dentistry Courses</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/enquiry">Admission Form</a>
            </li>
          </ul>
          <ul class="navbar-nav topmenuright">
            <!-- <li class="nav-item">
              <a class="nav-link" href="#">Careers</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link" href="/contact">Contact Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/enquiry">Enquiry Form</a>
            </li>
            <li class="nav-item">
              <!-- <a class="nav-link" href="#">Sitemap</a> -->
            </li>
          </ul>
    
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <i></i></a>
      </li>
      <li class="nav-item dropdown">
        <!-- <a class="nav-link" href="departments.html">Departments <i></i></a> -->
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Departments</a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="/anatomy">Anatomy</a>
          <a class="dropdown-item" href="/physiology">Physiology</a>
          <a class="dropdown-item" href="/biochemistry">Biochemistry</a>
          <a class="dropdown-item" href="/prosthetics">Prosthetics</a>
          <a class="dropdown-item" href="/conservative">Conservative Dentistry</a>
          <a class="dropdown-item" href="/pathology">Pathology</a>
          <a class="dropdown-item" href="/pharmacology">Pharmacology</a>
          <a class="dropdown-item" href="/oralpathology">Oral Pathology</a>
          <a class="dropdown-item" href="/generalmedicine">General Medicine</a>
          <a class="dropdown-item" href="/generalsurgery">General Surgery</a>
          <a class="dropdown-item" href="/pedodontics">Pedodontics</a>
          <a class="dropdown-item" href="/radiology">Radiology</a>
          <a class="dropdown-item" href="/orthodontics">Orthodontics</a>
          <a class="dropdown-item" href="/periodontics">Periodontics</a>
          <a class="dropdown-item" href="/communitydentistry">Community Dentistry</a>
          <a class="dropdown-item" href="/oral-maxillofacial-surgery">Oral &amp; Maxillofacial Surgery</a>
          <a class="dropdown-item" href="/oral-medicine">Oral Medicine</a>

        </div>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="comming-soon.html">Testimonials <i></i></a> 
      </li> -->
      <li class="nav-item">
        <a class="nav-link" href="/photo-gallery">Photo Gallery <i></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/comming-soon">Downloads<i></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/news-and-event">News & Events <i></i></a>
      </li>      
    </ul>
  </div>
</nav>
 </div>
</header>

@yield('content') 

<!-- Footer Start -->
<footer>
 <div class="container-fluid">
    
<div class="questions-footer text-center">
  Still have an questions. Please Ask Us! <a href="/enquiry">Enquire Now!</a>
</div>

<div class="row">
 
 <div class="col-md-3 col-sm-6">
  <p><img src="images/logo-footer.png" class="img-fluid" alt="Goenka Research Institute of Dental Science"></p>
  <div class="copy-txt">Copyright 2020 Goenka Research Institute of Dental Science.
All rights reserved.</div>
 </div>

<div class="col-md-3 col-sm-6">
  <h2>Get in Touch</h2>
  <p>Phone : +91 79 30511111<br>
Pethapur - Mahudi Road,<br>
Near G.G.S., Piplaj,<br>
Dist. Gandhinagar,<br>
Gujarat, India - 382610 </p>  
 </div>

<div class="col-md-6 col-sm-12">
  <h2>Departments</h2>
   <ul>
     <li> <a  href="anatomy.html">Anatomy</a></li>
        <li> <a  href="physiology.html">Physiology</a></li>
        <li> <a  href="biochemistry.html">Biochemistry</a></li>
        <li> <a  href="prosthetics.html">Prosthetics</a></li>
        <li> <a  href="conservative.html">Conservative Dentistry</a></li>
        <li> <a  href="pathology.html">Pathology</a></li>
        <li> <a  href="pharmacology.html">Pharmacology</a></li>
        <li> <a  href="oralpathology.html">Oral Pathology</a></li>
        <li> <a  href="generalmedicine.html">General Medicine</a></li>
        <li> <a  href="generalsurgery.html">General Surgery</a></li>
        <li> <a  href="pedodontics.html">Pedodontics</a></li>
        <li> <a  href="radiology.html">Radiology</a></li>
        <li> <a  href="orthodontics.html">Orthodontics</a></li>
        <li> <a  href="periodontics.html">Periodontics</a></li>
        <li> <a  href="communitydentistry.html">Community Dentistry</a></li>
        <li> <a  href="oral-maxillofacial-surgery.html">Oral &amp; Maxillofacial Surgery</a></li>
        <li> <a  href="oral-medicine.html">Oral Medicine</a></li>
   </ul>    
 </div>


</div>
    
 </div>
</footer>
</div>
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script> 
<script type="text/javascript" src="js/popper.min.js"></script> 
<script type="text/javascript" src="js/slick.js"></script> 
<script type="text/javascript" src="js/custom.js"></script>

@if (session('status'))
<script>
        $("#myModal").modal('show');
</script>
@endif

</body>
</html>