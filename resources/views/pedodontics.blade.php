@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Pedodontics  </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Pedodontics </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">Pedodontics aims at promoting and managing good dental health among children. This field of dentistry places additional emphasis on establishing a feeling of trust and confidence in the patient. It has been proven that poor dental health is often responsible for decline in school performance and unhealthy social interactions. Therefore paediatric dentists give advice on how to make teeth strong, the importance of healthy eating habits and other ways to prevent cavities.</p>
   <!-- <p>What puts this kind of dentistry in a different league from regular dentistry is the emphasis that this practice places on not only inculcating medical knowledge but also training the students on the necessity of establishing a feeling of trust and confidence in the patient. Pedodontics is mainly employed in treating children and maintaining the good health of their teeth. It has been proven that poor or deteriorating dental health is often responsible for decline in school performance and unhealthy social interactions.</p>
   <p>Therefore pediatric dentists give advice on how to make teeth strong the importance of developing healthy eating habits and other ways to prevent cavities from occurring.</p> -->
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>For child patients we have the pedodontics department with dental chairs & necessary equipments and all other instruments necessary for a happy and healthy treatment of children.

  </p>
  
  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/Pedodontics1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/Pedodontics2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
 </div>
</section>
@endsection