@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Biochemistry </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Biochemistry </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container">
   <p>The key function of the study of Biochemistry is to understand the functioning of various chemical processes in the human body. It establishes relationships between Metabolism, Nutrition, Homeostasis, Molecular Biology and Clinical Biochemistry.</p>
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>As mentioned earlier, the biochemistry and Physiology departments share a common lab which is totally equipped to execute teaching and practicals at the highest level.</p>
  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/Biochemistry1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/Biochemistry2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
  
 </div>
</section>
@endsection