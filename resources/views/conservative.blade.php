@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Conservative Dentistry  </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Conservative Dentistry </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">This branch of dental studies deals with various direct and indirect methods of conservation of original teeth. This includes employing dental fillings, root canal treatment, crowns, bridges, veneers, inlays and on-lays to functionally and aesthetically restore the capabilities of even a single tooth, unless extraction is the only option. The department of Conservative Dentistry and Endodontics caters to graduate students and offers a postgraduate program leading to a Master of Dental Surgery.</p>
   <!-- <p class="text-justify">Conservative dentistry includes various kinds of direct and indirect restorations and is concerned with the conservation of single teeth in the mouth. Conservative dentistry means the restoration functionally and aesthetically of patient’s original tooth, including dental fillings (new fillings and replacement), root canal treatment, crowns, bridges, veneers, inlays and onlays.</p>
   <p class="text-justify"> Dentistry is the branch of dentistry which is concerned with the conservation of teeth in the mouth. It embraces the practice of operative dentistry and endodontics, and includes various kinds of direct and indirect restorations of individual teeth in the mouth. The department of conservative dentistry and endodontics at rural dental college caters to graduate students at the school and offers a postgraduate program leading to a master of dental surgery.</p> -->
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>GRIDS has on campus, one clinical department with dental chairs having all necessary instruments for doing clinical work on patients.</p>
  <p>We also possess the latest phantom head Lab to teach students clinical works artificially. Our Lab strength is for one batch, which contains 60 students.</p>
  
  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/ConservativeDentistry2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/ConservativeDentistry3.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
 </div>
</section>

@endsection