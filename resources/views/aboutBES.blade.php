@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/facility-banner.png) left top no-repeat; background-size:cover;">
  <h1>Infrastructure</h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li>Infrastructure</li>
</ul>
</div>
</section>

<section class="AboutWrap clearfix">
  <div class="container">
    <!-- Nav tabs -->
     <ul class="nav nav-tabs">
       <li class="nav-item">
         <a class="nav-link active" data-toggle="tab" href="#Profile">Facilities <i></i></a>
       </li>
       <li class="nav-item">
         <a class="nav-link" data-toggle="tab" href="#ChairmanDesk">Campus <i></i></a>
       </li>
       <!-- <li class="nav-item">
         <a class="nav-link" data-toggle="tab" href="#PrincipalDesk">Hospital <i></i></a>
       </li> -->
       <!-- <li class="nav-item">
         <a class="nav-link" data-toggle="tab" href="#Affiliations">Affiliations <i></i></a>
       </li> -->
     </ul>
     
     <!-- Tab panes -->
     <div class="tab-content">
       <div class="tab-pane container active" id="Profile">
         
         <div class="row">
          <!-- <div class="col-lg-2 col-12"><img src="images/Mission.png" alt="Mission"></div> -->
          <div class="col-lg-12 col-12">
             <h2>Lecture Halls</h2>
             <p>4 lecture halls, with a combined area of 6400 sq. ft. adorn the institute. Each spacious lecture hall is equipped with adequate seating, a large greenboard and full audio-visual and multimedia projection capabilities.</p>
          </div>
         </div>
         
         <h2>Photography and Artist Room</h2>
         <p>The 400 sq. ft. dark room provides a hi-tech studio facility for clinical photography. This allows students to capture and develop imaging, as well as prepare slides, charts, models etc. in-house.</p>
         <!-- <ul>
          <li>To conduct courses leading to dental professional qualifications.</li>
          <li>To conduct courses leading to auxiliary dental professional qualifications.</li>
          <li>To provide opportunities for continuous professional development.</li>
          <li>To encourage students’ activities aimed at benefiting their character building.</li>
          <li>To improve quality of life of people by providing basic and advanced oral healthcare services.</li>
          <li>To foster research for advancement of dental practice and community well-being.</li>
         </ul> -->
         
         <h2>Amenities’ Area</h2>
         <p>This 3200 sq ft area has distinct zones for male students, female students, teaching staff and non-teaching staff. It consists of separate common rooms, locker rooms and change rooms. This facility offers ample opportunity for everyone at GRIDS to seek their own privacy as well as indulge in social interaction as and when needed.
        </p>
         <!-- <ul>
          <li>Attain excellence in dental health education.</li>
          <li>Serve the oral health care needs of all people with compassion irrespective of religion, caste or creed, particularly the poor and the needy.</li>
          <li>Inculcate moral values amongst students so as to mould them into doctors who would lead a principled life of honesty and integrity.</li>
          <li>Give a special emphasis on community oriented service through the graining and effective participation of members of the community.</li>
          <li>Acquire procedural skills in various aspects of dental surgery.</li>
          <li>Attain effective research skill and to stay abreast of the latest in dental research and technology. Through these activities each student who successfully completes the program will acquire the basic knowledge skills and judgment to competently practice dentistry.</li>
         </ul> -->
         <h2>Auditorium</h2>
         <p>The acoustically and ergonomically designed auditorium easily accommodates 500 people. The theatre-like room consists of plush seats, a welcoming reception counter, spacious green rooms, a large lobby and the latest audio-visual equipment.</p>


         <h2>Animal House</h2>
         <p>For research work on animals, we have our own animal house on campus. Adequate care is taken to nurture these animals in sanitary conditions, in a humane and gentle manner.</p>

         <h2>Examination Hall</h2>
         <p>This spacious 3600 sq ft hall can seat 250 students at a time, and is ideal for university and other examinations.</p>

         <h2>Staff Quarters</h2>
         <p>For the ease and convenience of our esteemed staff members, a separate lodging facility has been provided within the 5 acre area allotted to the campus.</p>

         <h2>Play Ground</h2>
         <p>The playground in the sprawling campus is ideal for basketball, football, cricket and volleyball. It has a dedicated jogging and cycling track too.</p>

         <h2>Central Stores</h2>
         <p>This 800 sq ft storage house has facilities like racks, refrigerators and compact storage systems, providing basic amenities and logistics support to its members.</p>

         <h2>Maintenance Room</h2>
         <p>This 1000 sq ft room is equipped with all the requisite facilities to maintain and repair dental chairs as well as other equipment in the college and hospital.</p>

         <h2>Medical Stores</h2>
         <p>This 300 sq ft shop maintains stock of various medicines and supplies that are required for dental procedures. This also makes procurement of prescribed drugs easier for patients of the dental hospital.</p>

         <h2>Compressor and Room for Gas Plant</h2>
         <p>A dedicated room accommodates required capacities of compressors, gas cylinders and other such allied equipment needed for dental procedures.</p>

         <h2>Pollution Control Measures</h2>
         <p>At GRIDS we believe in a ‘Green Globe’. On-campus plants for incineration and sewage water treatment are some of the steps towards this. Our lush, landscaped campus also ensures that residents are always surrounded by ample greenery.</p>
        </div>
       <div class="tab-pane container fade" id="ChairmanDesk">
         <div class="row">
           <div class="col-lg-12 col-12 text-justify">
              <h2>Library</h2>
              <p>The 8000 sq ft library at GRIDS is not merely a study room or a lounging room, but on the contrary, has several essentials required by students for daily learning. With a reception, waiting area, stocking area, photocopying area, locker facility, journal room, AV room and dedicated staff reading room, the library is equipped to seat 50% of total students’ strength.</p>
              <p>The Chief Librarian’s room ensures standardization of library materials, accurate logging of incoming and outgoing books and helpful staff within the library. With a wide reserve of over 1600 most recent books by Indian and International authors and 15 Computers at the students’ disposal, the library is truly the pride of the institute.</p>
           </div>
          </div>
       
       </div>
       <div class="tab-pane container fade" id="PrincipalDesk">
         <div class="row">
           <div class="col-lg-12 col-12 text-justify">
              ...
           </div>
         </div>  
       </div>
       <div class="tab-pane container fade" id="Affiliations">
           <div class="row">
             <div class="col-lg-6 col-md-6">
                 <center>
                   <img src="images/guj_logo.jpg">
                 </center>
             </div>
             <div class="col-lg-6 col-md-6">
               <center>
                 <img src="images/logo_affi.jpg">
               </center>
           </div>
           </div>
       </div>
     </div>
    
    
    
  </div>
 </section> 

<!-- Welcome Start -->
<!-- <section class="aboutwrap clearfix">
 <div class="container">
  <p>The BHOLARAM EDUCATION SOCIETY or BES was established in 2003 by the renowned industrialist, Shri Ramesh Goenka. This beginning was made to cater to a social and a national need- that of imparting quality education to the populace. It is the belief in this philosophy that set the foundation of the Delhi Public School (a part of the BES) at Gandhinagar in 2004 which was inaugurated by honourable Prime Minister of India Shri. Narendra Bhai Modi, then Chief Minister of Gujarat.</p>
  <p>Having taken this leap in the field of education, today the BES society has to its credit, 12 years of successful operations in the name of DPS Gandhinagar. Today DPS Gandhinagar boasts of more than 3200 students who have achieved major milestones in the field of education. This education society does not merely cater to nurturing the academic, moral and ethical dimensions of a child’s personality, but takes things and edge further by providing to the learner’s, an environment to experience competition. This prepares them for the real world outside. Several awards instituted by the society, ensure that the students foster a positive perspective towards competition and equip themselves to face the competition that awaits them.</p>
  <p>The group has since been establishing new standards in education, which are now being appreciated by students, parents and educators worldwide. Having thus cemented its footing in the field of education, BES embarked on another ambitious venture - that of imparting education in the field of dental sciences and Goenka Research Institute of Dental Science or GRIDS along with 100 Bedded Multi-speciality Hospital was conceived in the year 2010. With this new venture, BES aspired to embrace the needs of aspiring dentists and assist them in fulfilling their need for fine dental education. It aims for a perfect synthesis of: ancient and modern; Indian and international; traditional and innovatory. GRIDS meets the highest educational expectations in the rapidly growing community of dentistry. With this institution of GRIDS, BES offers to its student’s, opportunities for vocational education and professional development and echo the standards of achievement, quality and consistency laid down by its first venture-DPS Gandhinagar.</p>
 </div>
 <div class="container">
   <h2>Facilities:</h2>
 </div> -->
  
<p> <img src="images/about-img.png" alt="About"></p></section> 
@endsection