@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/news-banner.png) left top no-repeat; background-size:cover;">
  <h1>News &amp; Events</h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li>News &amp; Events</li>
</ul>
</div>
</section>
@if(count($news_events) > 0)

    @foreach($news_events as $news_event)
    @if($news_event->display_image=='left')
    <section class="newswrap clearfix">
    <div class="container newsdata">
      <div class="row">
      <div class="col-lg-6 col-sm-6 col-12"><center>
        <img src="images/{{$news_event->image}}" class="img-fluid">
      </center></div>
      <div class="col-lg-6 col-sm-6 col-12">
        <h2><i></i> {{$news_event->title}}.</h2>
        <p>{{$news_event->description}}</p>
      </div>   
      </div>
    </div>  
    </section> 
    @else
    <section class="newsdata1 clearfix">
    <div class="container">
      <div class="row">
      <div class="col-lg-6 col-sm-6 col-12 m-hide"><center>
        <img src="images/udaan.png" class="img-fluid" alt="UDAN FESTIVAL">
      </center></div>
      <div class="col-lg-6 col-sm-6 col-12">
      <h2><i></i> {{$news_event->title}}.</h2>
        <p>{{$news_event->description}}</p>
      </div>  
      <div class="col-lg-6 col-sm-6 col-12 d-hide"><center>
      <img src="images/{{$news_event->image}}" class="img-fluid">
      </center></div> 
      </div>
    </div>  
    </section>
    @endif
    @endforeach
@else
    <h3 style="text-align:center;">No Data Found</h3>   
@endif
<!-- Welcome Start -->

<!-- <section class="newswrap clearfix">
 <div class="container newsdata">
  <div class="row">
   <div class="col-lg-6 col-sm-6 col-12"><center>
     <img src="images/Conservative-Dentistry.png" class="img-fluid" alt="Conservative Dentistry">
   </center></div>
   <div class="col-lg-6 col-sm-6 col-12">
    <h2><i></i> Conservative Dentistry and Endodontic <span>CDE Program on 15th May,2019</span>.</h2>
    <p>Department of Conseravtive Dentistry and Endodontics, at Goenka Research Institute Of Dental Sciences, organized a CDE programme on 15th May, 2019. The first lecture was on "Strategic Planning in Conservative Dentistry and Endodontics ", by our guest speaker, Dr Akshay Langalia.</p>
   </div>   
  </div>
 </div>  
</section> 


<section class="newsdata1 clearfix">
 <div class="container">
  <div class="row">
   <div class="col-lg-6 col-sm-6 col-12 m-hide"><center>
     <img src="images/udaan.png" class="img-fluid" alt="UDAN FESTIVAL">
   </center></div>
   <div class="col-lg-6 col-sm-6 col-12">
    <h2><i></i> UDAN FESTIVAL AT  <span>GRIDS</span>.</h2>
    <p>“We Organized Udaan as a Pre Uttarayan Celebration on 06/01/2018 Saturday with Dj, Dance, Kitefying, Food </p>
   </div>  
   <div class="col-lg-6 col-sm-6 col-12 d-hide"><center>
     <img src="images/udaan.png" class="img-fluid" alt="UDAN FESTIVAL">
   </center></div> 
  </div>
 </div>  
</section>


<section class="newswrap clearfix">
 <div class="container newsdata">
  <div class="row">
   <div class="col-lg-6 col-sm-6 col-12"><center>
     <img src="images/Blood-Donation.png" class="img-fluid" alt="Blood Donation">
   </center></div>
   <div class="col-lg-6 col-sm-6 col-12">
    <h2><i></i> Blood Donation Camp in Rajbhavan, <span>Gandhinagar</span>.</h2>
    <p>“Blood Donation Camp was organized at Rajbhavan, Gandhinagar to celebrate the Birthday of Our Honorable Governor, Shri O. P. Kohli on 9/8/2017. Goenka Campus proudly became part of the camp. Students and Staff members from Goenka Hospital, Goenka Research Institute of Dental Science and Manjushree Research Institute...</p>
   </div>   
  </div>
 </div>  
</section> -->
@endsection