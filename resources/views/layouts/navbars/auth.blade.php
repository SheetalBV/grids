<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('paper') }}/img/logo-small.png">
            </div>
        </a>
        <a href="" class="simple-text logo-normal">
            {{ __('Grids') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <!-- <li class="{{ $elementActive == 'dashboard' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'dashboard') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li> -->

            <li>
                <a>
                    <i></i>
                    <p>Master</p>
                </a>
            </li>

             <!--Vendor Master-->
             <li class="{{ $elementActive == 'user' || $elementActive == 'profile' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#laravelExamples1">
                    <i class="nc-icon nc-app"></i>
                    <p>
                        {{ __('Slider') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="laravelExamples1">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'profile' ? 'active' : '' }}">
                            <a href="/slider_add">
                                <span class="sidebar-mini-icon">{{ __('+') }}</span>
                                <span class="sidebar-normal">{{ __('Add ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'user' ? 'active' : '' }}">
                            <a href="/slider_view">
                                <span class="sidebar-mini-icon">{{ __('#') }}</span>
                                <span class="sidebar-normal">{{ __('View') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            
            <!-- News & Events-->
            <li class="{{ $elementActive == 'user' || $elementActive == 'profile' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#laravelExamples4">
                    <i class="nc-icon nc-app"></i>
                    <p>
                        {{ __('News & Events') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="laravelExamples4">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'profile' ? 'active' : '' }}">
                            <a href="/news_events_add">
                                <span class="sidebar-mini-icon">{{ __('+') }}</span>
                                <span class="sidebar-normal">{{ __('Add ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'user' ? 'active' : '' }}">
                            <a href="/news_events_view">
                                <span class="sidebar-mini-icon">{{ __('#') }}</span>
                                <span class="sidebar-normal">{{ __('View') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <!-- Photo Gallery-->
            <li class="{{ $elementActive == 'user' || $elementActive == 'profile' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#laravelExamples5">
                    <i class="nc-icon nc-app"></i>
                    <p>
                        {{ __('Photo Gallery') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="laravelExamples5">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'profile' ? 'active' : '' }}">
                            <a href="/photo_gallery_add">
                                <span class="sidebar-mini-icon">{{ __('+') }}</span>
                                <span class="sidebar-normal">{{ __('Add ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'user' ? 'active' : '' }}">
                            <a href="/photo_gallery_view">
                                <span class="sidebar-mini-icon">{{ __('#') }}</span>
                                <span class="sidebar-normal">{{ __('View') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <!-- -->
            <!-- <li class="{{ $elementActive == 'user' || $elementActive == 'profile' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#laravelExamples5">
                    <i class="nc-icon nc-app"></i>
                    <p>
                        {{ __('Client Speak') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="laravelExamples5">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'profile' ? 'active' : '' }}">
                            <a href="/client_speak_add">
                                <span class="sidebar-mini-icon">{{ __('+') }}</span>
                                <span class="sidebar-normal">{{ __('Add ') }}</span>
                            </a>
                        </li>
                        <li class="{{ $elementActive == 'user' ? 'active' : '' }}">
                            <a href="/client_speak_view">
                                <span class="sidebar-mini-icon">{{ __('#') }}</span>
                                <span class="sidebar-normal">{{ __('View') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li> -->
            <li class="{{ $elementActive == 'user' || $elementActive == 'profile' ? 'active' : '' }}">
                <a data-toggle="collapse" aria-expanded="true" href="#laravelExamples5">
                    <i class="nc-icon nc-app"></i>
                    <p>
                        {{ __('Enquiries') }}
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse" id="laravelExamples5">
                    <ul class="nav">
                        <li class="{{ $elementActive == 'user' ? 'active' : '' }}">
                            <a href="/view_enquiry">
                                <span class="sidebar-mini-icon">{{ __('#') }}</span>
                                <span class="sidebar-normal">{{ __('View') }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
           
            {{-- <li class="{{ $elementActive == 'icons' ? 'active' : '' }}">
            <a href="{{ route('page.index', 'icons') }}">
                <i class="nc-icon nc-diamond"></i>
                <p>{{ __('Icons') }}</p>
            </a>
            </li>
            <li class="{{ $elementActive == 'map' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'map') }}">
                    <i class="nc-icon nc-pin-3"></i>
                    <p>{{ __('Maps') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'notifications' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'notifications') }}">
                    <i class="nc-icon nc-bell-55"></i>
                    <p>{{ __('Notifications') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'tables' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'tables') }}">
                    <i class="nc-icon nc-tile-56"></i>
                    <p>{{ __('Table List') }}</p>
                </a>
            </li>
            <li class="{{ $elementActive == 'typography' ? 'active' : '' }}">
                <a href="{{ route('page.index', 'typography') }}">
                    <i class="nc-icon nc-caps-small"></i>
                    <p>{{ __('Typography') }}</p>
                </a>
            </li> --}}

        </ul>
    </div>
</div>
