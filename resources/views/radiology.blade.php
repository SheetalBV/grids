@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Radiology   </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Radiology  </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">No clinical dental treatment is meaningful without proper diagnostic work. Hence, radiology, or the art and science of diagnosis, forms the foundation of clinical dentistry. Since Maxillofacial Radiology and Craniofacial Imaging also come under the purview of this department, it plays a vital role in the planned treatment for patients.</p>
   <!-- <p> No clinical dental treatment is meaningful without a proper diagnostic work up or appropriate treatment planning. The department of OMDR gets more importance because of these facts. Since maxillofacial radiology and craniofacial imaging also come under the preview of this department, it plays a vital role in the planned treatment for patients.</p> -->
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>The department has dental chairs and all the latest equipments required for the in depth study of radiology. Also we have radiology department is in the same department having Digital OPG, Intraoral X-Ray with RVG facility and Dark Room with automatic X-Ray developer unit.</p>
  
  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/Radiology1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/Radiology2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
 </div>
</section>
@endsection