@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Prosthetics  </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Prosthetics </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">Prosthetic Dentistry & Dental Material are allied subjects that are taught for all 5 years of the course, with 2 university exams held during this period. Understanding of dentures, crowns, bridges, implants, tissue replacement, preparation of veneers and smile design are crucial knowledge for any aspiring dentist.</p>
   <!-- <p class="text-justify">Dental Material is taught from first year & students appear for university exam in 2nd year.</p>
   <p class="text-justify">Prosthetic Dentistry is taught from 1st year BDS. In second year students appear for preclinical practical exam where they learn the laboratory aspect of making acrylic dentures.</p>
   <p class="text-justify">In fifth year BDS, students appear for clinical prosthetic university examination. So this subject is taught for five years. Prosthetic & Dental Material are allied subjects.</p> -->
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>Prosthetics is the main subject of the Dental College, we have major three departments:</p>
  <ul>
    <li class="li_display">Clinical Prosthetic department with several equipments.</li>
    <li class="li_display">Material Lab with cabals which have gas burner facility. This Lab is also used for teaching prosthetic subject practical to dental students.</li>
    <li class="li_display">We have also Polymer room, Wax room, Plaster room, casting room & Ceramic furnace room with all latest Lab equipments to do prosthetic lab works.</li>
   </ul>
   <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/Prosthetics1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/Prosthetics2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
 </div>
</section>
@endsection