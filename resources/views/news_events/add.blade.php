@extends('layouts.app', [
'class' => '',
'elementActive' => 'tables'
])

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-plain">
                <h4 class="card-title">Add News & Events</h4>
                <form method="post" action="{{ URL('news_events_store') }}" enctype='multipart/form-data'>
                @csrf
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <h6>Image<sup class="star">*</sup></h6>
                            <input type="file" class="form-control" accept="image/*" id="image" name="image" placeholder="" required>
                        </div>                        
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <h6>Display image<sup class="star">*</sup></h6>                            
                            <label class="radio-inline">
                                <input type="radio" name="imageDisplay" value="left">  <b>Left</b>
                            </label>&nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="radio-inline">
                                 <input type="radio" name="imageDisplay" value="right"> <b>Right</b>
                            </label>
                        </div>                        
                    </div>    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <h6>Title<sup class="star">*</sup></h6>
                            <input type="text" class="form-control" id="title" name="title" placeholder="" required>
                        </div>                        
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <h6>Description<sup class="star">*</sup></h6>                            
                            <textarea class="form-control" id="description" name="description" required></textarea>
                        </div>                        
                    </div>
                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function () {
        $("#image").change(function() {

            debugger;
            var val = $(this).val();
            switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
                case 'jpeg': case 'jpg': case 'png':
                    //alert("an image");
                    break;
                default:
                    $(this).val('');
                    // error message here
                    alert("File is invalid");
                    break;
            }
        });
    });
</script>