@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'tables'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header">
                    
                        <div class="row">
                            <div class="col-md-10">
                            <h4 class="card-title">News & Events </h4> 
                            </div>

                            <div class="col-md-2">
                                
                                    <button class="btn btn-outline-primary btn-round" onclick="location.href='news_events_add'">
                                        <i class="fa fa-plus"></i> News & Events
                                      </button>
                            </div>
                            
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table"  id="datatable">
                                <thead class=" text-primary">                                   
                                    <th>Sr.No.</th>
                                    <th>Image</th>
                                    <th>Display Image</th>
                                    <th>Title</th>                                    
                                    <th>Description</th>
                                    <th>Created On</th>                                   
                                    <th>Action</th>                                   
                                </thead>
                                <tbody>
                                    @foreach($news_events as $key => $ns)
                                    <tr>
                                        <td>{{($key+1)}}</td>  
                                        <td><a href='images/{{$ns->image}}' target="_blank">View</a></td>                                      
                                        <td>{{$ns->display_image}}</td>
                                        <td>{{$ns->title}}</td>                                        
                                        <td>{{$ns->description}}</td>
                                        <td>{{$ns->createdon}}</td>
                                        <td>
                                            <a href='news_events_edit?id={{$ns->ID}}'><i class="fa fa-pencil-square-o"></i></a>
                                            <a href='news_events_delete?id={{$ns->ID}}'><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>  
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<!-- <script>
    $(document).ready(function () {
        $('#escalation').DataTable();
    });
</script> -->