@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Orthodontics  </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Orthodontics </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">Orthodontics deals with the development and positional anomalies (including alignment) of the teeth and the jaws as they not only affect oral health but also the physical and mental well-being of the patient.</p>
   <!-- <p>In the study of orthodontics, thus emphasis is laid on the maintenance of oral, physical and mental health of patient.</p> -->
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>This department has all the latest technology & equipments to treat Malocclusion likewise prolinaction of teeth, crowding of teeth space problem etc. Different type of treatment option are available likewise myofunctional, removable plates, fixed bracket, hear options esthetic bracket available.</p>
 
  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/Orthodontics1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/Orthodontics2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
 </div>
</section>
@endsection