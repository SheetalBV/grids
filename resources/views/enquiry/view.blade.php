@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'tables'
])

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header">
                    
                        <div class="row">
                            <div class="col-md-10">
                            <h4 class="card-title">Enquiries </h4> 
                            </div>

                            <!-- <div class="col-md-2">
                                
                                    <button class="btn btn-outline-primary btn-round" onclick="location.href='slider_add'">
                                        <i class="fa fa-plus"></i> Banner
                                      </button>
                            </div> -->
                            
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table"  id="datatable">
                                <thead class=" text-primary">                                   
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Telephone</th>
                                    <th>Mobile</th>
                                    <th>Age</th>                                    
                                    <th>Address1</th>
                                    <th>Address2</th>
                                    <th>State</th>                                    
                                    <th>Zipcode</th>
                                    <th>Country</th>
                                    <th>Enquiry</th>
                                    <th>Created On</th> 
                                    <th>Action</th>                                                                      
                                </thead>
                                <tbody>
                                    @foreach($enquiries as $key => $enquiry)
                                    <tr>
                                        <td>{{($key+1)}}</td>                                        
                                        <td>{{ $enquiry->first_name}} {{ $enquiry->last_name}}</td>
                                        <td>{{ $enquiry->email}}</td>
                                        <td>{{ $enquiry->telephone}}</td>
                                        <td>{{ $enquiry->mobile}}</td>
                                        <td>{{ $enquiry->age}}</td>
                                        <td>{{ $enquiry->address1}}</td>
                                        <td>{{ $enquiry->address2}}</td>
                                        <td>{{ $enquiry->state}}</td>
                                        <td>{{ $enquiry->zipcode}}</td>
                                        <td>{{ $enquiry->country}}</td>
                                        <td>{{ $enquiry->enquiry}}</td>                                        
                                        <td>{{$enquiry->createdon}}</td>  
                                        <td>                                            
                                            <a href='enquiry_delete?id={{$enquiry->ID}}'><i class="fa fa-trash"></i></a>
                                        </td>                                      
                                    </tr>  
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
