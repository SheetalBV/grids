@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Oral &amp; Maxillofacial Surgery   </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Oral &amp; Maxillofacial Surgery  </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">This program is approved and accredited by the Dental Council of India. The Undergraduate Program deals with extraction of teeth, the complications arising and their subsequent management, and the management of medical emergencies on the dental chair. The Postgraduate Program begins with complicated extraction and goes on to involve OPD and OT cases as well.</p>

   <!-- <p> This is program is approved and accredited by the Dental Council of India.</p>
<p>The section of oral & Maxillofacial surgery has a varied scope. The under graduate program deals with extraction of teeth and the complications arising and the subsequent management, along with the management of medical emergencies on the dental chair. The post-graduate program beginning with complicated extraction goes on to involve outpatient department cases as well as patients in the wards and operation theatres. Along with this various research projects to train the students in research methodology are allotted to the students.
</p> -->
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<!-- <section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>The department has dental chairs and all the latest equipments required for the in depth study of radiology. Also we have radiology department is in the same department having Digital OPG, Intraoral X-Ray with RVG facility and Dark Room with automatic X-Ray developer unit.</p>
  
  <p><center>
    <img src="images/facilitate.png" class="img-fluid" alt="facilitate ">
  </center></p>
 </div>
</section> -->

@endsection