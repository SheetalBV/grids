@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/contact-banner.png) left top no-repeat; background-size:cover;">
  <h1>Enquiry Form</h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li>Enquiry Form</li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="EnquiryWrap clearfix">
 <div class="container">
 <form action="{{ URL('store_enquiry') }}" method="post" id="enquiryForm" name="enquiryForm">
 <div class="row">   
   @csrf
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="text" name="f_name" id="f_name" placeholder="First Name" required>
    </div>
   </div>
   
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="text" name="l_name" id="l_name" placeholder="Last Name" required>
    </div>
   </div>
   
    <div class="col-lg-6 col-sm-6 col-12">
        <div class="form-group">
            <input class="form-control" type="text" name="age" id="age" placeholder="Age" required>
        </div>
   </div>
   
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="tel" name="telephone" id="telephone" placeholder="Telephone" required>
    </div>
   </div>
   
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="tel" name="mobile" id="mobile" placeholder="Mobile Number" required>
    </div>
   </div>
   
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="tel" name="fax_no" id="fax_no" placeholder="Fax" required>
    </div>
   </div>
   
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="email" name="email" id="email" placeholder="E-mail Address" required>
    </div>
   </div>
   
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="text" name="address1" id="address1" placeholder="Address 1" required>
    </div>
   </div>
      
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="text" name="address2" id="address2" placeholder="Address 2" required>
    </div>
   </div>
   
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="text" name="state" id="state" placeholder="State" required>
    </div>
   </div>
   
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="number" name="zipcode" id="zipcode" placeholder="Zip Code" required> 
    </div>
   </div>
   
   <div class="col-lg-6 col-sm-6 col-12">
    <div class="form-group">
     <input class="form-control" type="text" name="country" id="country" placeholder="Country" required>
    </div>
   </div>
   
   <div class="col-lg-12 col-sm-12 col-12">
    <div class="form-group">
     <textarea class="form-control message" name="enquiry" id="enquiry" placeholder="Enquiry" required></textarea>
    </div>
   </div>
   
   <div class="col-lg-12 col-sm-12 col-12">
    <div class="form-group">
     <button class="btn-submit" type="submit">submit</button>
    </div>
   </div>   
  </div>
  </form>
 </div>
</section> 

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Enquiry Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Enquiry form submitted successfully, we will get back to you shortly!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
@endsection