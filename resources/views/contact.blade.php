@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/contact-banner.png) left top no-repeat; background-size:cover;">
  <h1>Contact us</h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li>Contact us</li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="contactwrap clearfix">
 <div class="container">
 <div class="row">
  <div class="col-lg-6 col-sm-6 col-12">
    <h2>Address</h2>
    <p>Pethapur - Mahudi Road, Near G.G.S., Piplaj, Dist. Gandhinagar, Gujarat,  India - 382610</p>
    <p>Phone  :  +91 79 30511111, +91-9876543210</p>
    <p>Fax  :      +91 79 30511044</p>
    <p>E-mail  :  info@grids.co.in</p>
  </div>
  <div class="col-lg-6 col-sm-6 col-12">
   <h2>Address</h2>
   <div class="mapb">
     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1779.708068112753!2d72.68236419959118!3d23.32610111797605!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc3ed0ef5f3611394!2sGoenka%20Hospital!5e0!3m2!1sen!2sin!4v1604641091007!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
   </div>
  </div>
  
  </div>
 </div>
</section> 

@endsection