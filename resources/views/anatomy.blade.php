@extends('front_layouts.app')

@section('content')

<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Anatomy </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Anatomy</li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container">
   <p>In the first year, our students learn about the whole human body including dissection of cadavers, microscopic observation of tissues and the understanding of the cranium. The human body is the most complex machine made of millions of moving parts, and the intent is to build fascination and respect for knowledge related to it.</p>
   <p><center><a href="/courses"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>To facilitate an in-depth understanding of these, the department houses a dissection hall, histology lab, anatomy museum, staff & seminar rooms. Dissection hall consists of dissection tables, various charts for teaching. Histology lab consists of histology slides & microscopes for teaching. General Anatomy museum is designed to demonstrate & to teach hard & soft tissues of human body. Museum is equipped with bones, skeleton, and various models of viscera’s.</p>
  <p><center>
    <img src="images/facilitate.png" class="img-fluid" alt="facilitate ">
  </center></p>
 </div>
</section>
@endsection