@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/courses-banner.png) left top no-repeat; background-size:cover;">
  <h1>BDS Course</h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li>BDS Course</li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="CourseWrap clearfix">
 <div class="container">
  <p>The health of our teeth and mouth are linked to overall health and well-being in a number of ways. The ability to chew and swallow our food is essential for obtaining the nutrients we need for good health. Apart from the impact on nutritional status, poor dental health can also adversely affect speech and self-esteem.</p>
  <p>We are at the beginning of a new decade, a new century, and a new millennium. This new age has brought with it added awareness towards dental care and oral hygiene. The transcending change in social status of people and lifestyle, the demand oral and dental care is on the rise.</p>
  <p>As a worthwhile to this demand, several youth have begun to assess the possibility of a career in this field. Thus, there is a need to educate and train an expert dental workforce in India to adequately and efficiently provide dental care to a population growing in size and diversity.</p>
  <p>A fundamental purpose of dental education is to develop health professionals who will maintain and improve the oral health status of individuals and populations. GRIDS pursues dental education based on some common guidelines laid down for all BDS courses nationwide by Dental Council of India and the Gujarat University. The basic requisite for becoming a dentist requires a Bachelor’s Degree in Dental Science. GRIDS offer under graduate dental training program leading to BDS Degree and Post Graduate Dental Training program leading to MDS Degree. Bd.S degree shall be 5 years durations minimum. During this period, the students shall be required to engage in full time study at the dental college.</p>
  <p>During the 5 years of study 2 years are for theoretical and basic laboratory study and last 3 years in clinical subject with practical on patient .During the period of these years, GRIDS puts its students through vigorous sessions of certified training which ensures not only the academic development of the student but also a grooming of the ethics that are the ethos of this profession.</p>
  <p>During one’s years in GRIDS, students with a specific goal in life and dynamic approach to attain it are culminated together and are instilled with the spirit of enquiry, ability to reason and mastery in the art and science of dentistry. The nurturing of talent in the field of dentistry on the firm foundations of global competencies and inviolable ethical principles is the pivotal aspiration.</p>
  <p></p>
 </div>
 
</section> 

<div class="CourseBox clearfix">
 <div class="container">
  <h2>Fees</h2>
  <p>From academic year 2010-2011, new fee structure has been approved for our college by the fee Regulatory Committee, Gujarat. The fee has been decided by R.J. Shah committee in accordance to the state law of “The Gujarat Professional Medical Educational Colleges or Institutions (Regulation of Admission and Fixation of Fees) Act. 2007. Under this law, special provision for regulation of admission in the professional Medical educational colleges or institutions is provided and fee structure fixed. The R.J. Shah committee has decided the fees and as per their guidance the fees will be charged from the student.</p>
  <div class="text-center">
   <a href="/enquiry">Apply online</a> <a href="/enquiry">Download Admission Form</a>
  </div>
 </div>
</div>
 
<div class="CourseIndent clearfix">
 <div class="container">
  <div class="CourseDetail clearfix">
   <div class="CourseLeft">First Year</div>
   <div class="CourseRight">General Human Anatomy including Embryology and Histology, General Human Physiology & Biochemistry, Dental Anatomy Embryology & Oral Histology, Dental Materials, Pre-Clinical Prosthodontics & Crown & Bridges</div>
   
   <div class="CourseLeft">Second Year</div>
   <div class="CourseRight">General Pathology and Microbiology including Parasitology, General and Dental Pharmacology, Oral Pathology & Oral Microbiology, Dental materials, Preclinical Conservative Dentistry & Pre-Clinical Prosthodontics & Crown & Bridges</div>
   
    <div class="CourseLeft">Third Year</div>
   <div class="CourseRight">General Medicine, General Surgery, Oral Pathology and Microbiology, Conservative Dentistry & Endodontics, Oral & maxillofacial Surgery, Oral Medicine & Radiology, Orthodontics & Dentofacial Orthopeadics, Pediatric & Preventive Dentistry, Periodontology, Prosthodontics and Crown & Bridges</div>
   
   <div class="CourseLeft">Fourth Year	</div>
   <div class="CourseRight">Oral Medicine & Radiology, Paediatric & Preventive Dentistry, Orthodontics & Dentofacial Orthopaedics, Periodontology</div>
   
    <div class="CourseLeft">Fifth Year</div>
   <div class="CourseRight">Oral & Maxillofacial Surgery, Removable Partial Dentures, Conservative Dentistry and Endodontics, Public Health Dentistry</div>
   
   
  </div>
  </div>
</div>
@endsection