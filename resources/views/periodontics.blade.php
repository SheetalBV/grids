@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Periodontics   </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Periodontics  </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">Periodontia includes largely the maintenance of health of the tissues surrounding the teeth. This department specializes not only in oral prophylaxis, but also in procedures related to restoration of damaged gums, bones and other parts of tissue surrounding the teeth.</p>
   <p><center><a href="courses.html"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>To facilitate the study of periodontics, the department has been provided substantially with dental chairs and latest equipments. Here we treat different types of periodontal problem. To name sum we treat the ailments of gum swelling and perform bleeding gums periodontal surgery.</p>
  
  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/periodontics.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/Periodontics1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
 </div>
</section>
@endsection