@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/bannr.png) left top no-repeat; background-size:cover;">
  <h1>Pathology  </h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="/">Home</a></li>
  <li><a href="#">Departments</a></li>
  <li>Pathology </li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="welcomegide clearfix">
 <div class="container text-justify">
   <p class="text-justify">Pathology involves the study and diagnosis of disease through the examination of surgically removed organs, tissues (biopsy samples) and bodily fluids. Studying about the basics of this branch of medicine allows students to better understand the causes and effects of diseases on the human body.</p>
   <!-- <p class="text-justify">Preclinical conservative lab consists of phantom heads which are dummy models. Students learn to treat carious teeth on these models before they work on the patients. This is a second year subject.</p> -->
   <p><center><a href="/courses"><img src="images/bds-course.png" class="img-fluid" alt="GRIDS"></a></center></p>
   
 </div>
</section>


<section class="innerdata clearfix">
 <div class="container">
  <h2>Infrastructure</h2>
  <p>There is a huge lab for the study of this subject which is also shared by the students for the study of Microbiology. Lab tables are designed with sinks and rack with required chemicals. Preparation is also there for preparing different solutions for doing the practicals.</p>

  <div class="row">
    <div class="col-sm-6">
      <p><center>
        <img src="images/Pathology1.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
    <div class="col-sm-6">
      <p><center>
        <img src="images/Pathology2.jpg" class="img-fluid" alt="facilitate ">
      </center></p>
    </div>
  </div>
 </div>
</section>
@endsection