@extends('front_layouts.app')

@section('content')
<!-- Hero Start -->
<section class="inner-banner clearfix" style="background:url(images/contact-banner.png) left top no-repeat; background-size:cover;">
  <h1>About GRIDS</h1>
</section>

<!-- Breadcrumb -->
<section class="breadwrap clearfix">
<div class="container">
 <ul class="breadcrumb">
  <li><a href="index.html">Home</a></li>
  <li>About GRIDS</li>
</ul>
</div>
</section>


<!-- Welcome Start -->
<section class="AboutWrap clearfix">
 <div class="container">
   <!-- Nav tabs -->
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#Profile">Profile <i></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#ChairmanDesk">Chairman's Desk <i></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#PrincipalDesk">Principal's Desk <i></i></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#Affiliations">Affiliations <i></i></a>
      </li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane container active" id="Profile">
        
        <div class="row">
         <div class="col-lg-2 col-12"><img src="images/Mission.png" alt="Mission"></div>
         <div class="col-lg-10 col-12">
            <h2>Mission of GRIDS</h2>
            <p>To prepare, through excellence in educational experience, globally competent oral health care professionals with the highest ethical standards combined with great sensitivity to local expectations and social values.</p>
            <p>Professionals for science based ethical practice, which is sensitive to local expectations and social values.</p>
         </div>
        </div>
        
        <h2>Objectives</h2>
        <ul>
         <li>To conduct courses leading to dental professional qualifications.</li>
         <li>To conduct courses leading to auxiliary dental professional qualifications.</li>
         <li>To provide opportunities for continuous professional development.</li>
         <li>To encourage students’ activities aimed at benefiting their character building.</li>
         <li>To improve quality of life of people by providing basic and advanced oral healthcare services.</li>
         <li>To foster research for advancement of dental practice and community well-being.</li>
        </ul>
        
        <h2>The institutions approach to this mission is to</h2>
        <ul>
         <li>Attain excellence in dental health education.</li>
         <li>Serve the oral health care needs of all people with compassion irrespective of religion, caste or creed, particularly the poor and the needy.</li>
         <li>Inculcate moral values amongst students so as to mould them into doctors who would lead a principled life of honesty and integrity.</li>
         <li>Give a special emphasis on community oriented service through the graining and effective participation of members of the community.</li>
         <li>Acquire procedural skills in various aspects of dental surgery.</li>
         <li>Attain effective research skill and to stay abreast of the latest in dental research and technology. Through these activities each student who successfully completes the program will acquire the basic knowledge skills and judgment to competently practice dentistry.</li>
        </ul>

        <!-- Date : 14/01/2021 -->
        <h2>About Bholaram Education Society (BES)</h2>
        <ul>
         <li>Bholaram Education Society (BES) was established in 2003 by the renowned industrialist, Shri Ramesh Goenka.</li>
         <li>This beginning was made to cater to a social and a national need- that of imparting quality education to students.</li>
         <li>It is the belief in this philosophy that set the foundation of the Delhi Public School (a part of the BES) at Gandhinagar in 2004 which was inaugurated by honourable Prime Minister of India Shri. Narendra Modi, then Chief Minister of Gujarat.</li>
         
         <li>Having taken this leap in the field of education, today the BES society has to its credit, decades of successful operations in the name of DPS Gandhinagar.</li>
         <li>Today DPS Gandhinagar boasts of more than 5000 students who have achieved major milestones in the field of education.</li>
         <li>This institution does not merely cater to nurturing the academic, moral and ethical dimensions of a child’s personality, but takes things further by providing learners an environment of healthy competition. This prepares them for the real world outside.</li>
         <li>Several awards instituted by the society, ensure that the students foster a positive perspective towards competition and equip themselves to face the challenges that await them.</li> 
         <li>The Group has since been establishing new standards in education, which are now appreciated by students, parents and educators worldwide.</li>
         <li>Having thus cemented its footing in the field of education, BES embarked on another ambitious venture - that of imparting education in the field of Dental Sciences and Goenka Research Institute of Dental Science (GRIDS) along with a 100 bedded super-specialty hospital was conceived in the year 2010. </li> 
         <li>With this new venture, BES aspires to embrace the needs of budding dentists and assist them in fulfilling their need for the finest dental education.</li>
        </ul>
        
      </div>
      <div class="tab-pane container fade" id="ChairmanDesk">
        <div class="row">
          <div class="col-lg-12 col-12 text-justify">
             <p>Today dentistry is successful in occupying a distinct sphere of its own in the vast expanse of medical world. Having gained recognition as a specialized vista, this field of medical study has come up with immense scopes. Harping on the prospect and personal interest in this particular medical principle, umpteen numbers of students are taking up dentistry in their bachelors’ degree.</p>
             <p>In this rapidly evolving age of dental education it remains essential for doctors to understand not only the significance of dental care and importance of oral hygiene, but also keep abreast with research and latest developments.</p>
             <p>THE GRIDS program in dental education and care is designed to foster excellence in the future leaders of dental care. Thus, bearing this in mind, the focus of our educational program is not merely the transfer of information but the transformation of the learner in a culture providing that ingenious combination of support and challenge that leads to education.</p>
             <p>We at GRIDS have designed a program to provide a broad, rigorous and flexible foundation that will enable students to reach their highest potential of achievements. All of the programs reflect our philosophy that students learn best when developing thinking, questioning, and doing skills simultaneously. Our curriculum is well structured, based on a strong foundation program that provides the vocabulary and technical skills necessary for further and more specialized study. Students are trained to develop sensitivity to the world of the patient and to the social contexts in which medicine is practiced.</p>
             <p>In this endeavor, we attempt to launch an entirely new medical school curriculum based on integrated and interdisciplinary teaching of basic sciences and clinical medicine. With state of the art facilities that encompass lecture halls, an elaborate library, hi-tech labs, student common room, cafeterias, hostel facilities along with audio-visual space for the students to enjoy their study better and learn more.</p>
             <p>Furthermore the Faculty of Dentistry is equipped with paragon and seasoned teachers who are pioneers and leaders in their own fields. For gaining practical knowledge, the students are furnished with a comprehensive dental clinic which gives them the opportunity to transcend their knowledge into practical skills.</p>
             <p>Holding fast to cherished educational principles, we firmly believe we can forge a new age in the field of dental medical care through our pioneering work in the field of dentistry.</p>
             <p>I’m honored to get this opportunity to share my feelings and we look forward to having you as a part of our family.</p>

             <p class="text-right">Mr. Apoorva Goenka,<br>CHAIRMAN</p>
             <p></p>
          </div>
         </div>
      
      </div>
      <div class="tab-pane container fade" id="PrincipalDesk">
        <div class="row">
          <div class="col-lg-12 col-12 text-justify">
             <p> As principal of GRIDS I welcome you to the virtual world of GRIDS.These are exciting and challenging times for dentistry. We now know that much of dental disease can be prevented and we have at our disposal a wide array of techniques to alleviate pain and treat disease. The need for further improvements stimulates further research into better ways of preventing and treating dental disease and has also led us to develop new teaching methods for the dental student of today.</p>
             <p>At GRIDS, we are meeting these challenges with undergraduate and postgraduate courses designed to provide both the scientific background and the practical skills needed to make the best use of new materials and put into practice the most up-to-date advances in dentistry. Our strong research program intend to contribute both to our basic understanding of oral diseases and to applied aspects of their prevention and treatment.</p>
             <p>These web pages are intended to introduce you to our range of teaching and research activities and provide links to sources of further information. I hope you will take pleasure in seeing what we have to offer.</p>
             <p class="text-right"> Dr. Shweta Kumar Swami,<br>Professor & HOD, Prosthetic</p>
          </div>
        </div>  
      </div>
      <div class="tab-pane container fade" id="Affiliations">
          <div class="row">
            <div class="col-lg-6 col-md-6">
                <center>
                  <img src="images/guj_logo.jpg">
                </center>
            </div>
            <div class="col-lg-6 col-md-6">
              <center>
                <img src="images/logo_affi.jpg">
              </center>
          </div>
          </div>
      </div>
    </div>
   
   
   
 </div>
</section> 
@endsection