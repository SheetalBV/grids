<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsEvents extends Model
{
    use HasFactory;
    public $timestamps = false; 
    protected $table = 'news_events';

    protected $fillable = [
        'title','image','description','display_image','createdon'
    ];
}
