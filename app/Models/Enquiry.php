<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    use HasFactory;
    public $timestamps = false; 
    protected $table = 'enquiries';

    protected $fillable = [
    'first_name','last_name','age','telephone','mobile','fax_no','email','address1','address2','state','zipcode','country','enquiry', 'createdon'
    ];

}
