<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PhotoGallery;
use DB;

class PhotoGalleryController extends Controller
{
    public function index()
    {
        $photo_gallery = PhotoGallery::orderBy('ID', 'DESC')->get();    
        return view('photo_gallery.view',compact('photo_gallery'));
    }

    public function add()
    {
        return view('photo_gallery.add');
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');
        $photo_gallery = PhotoGallery::where('ID',$id)->first(); 
        return view('photo_gallery.edit',compact('id','photo_gallery'));
    }

    public function store(Request $request)
    {  
        //dd($request->all());

        $imageName = '';              
        if ($request->hasFile('image')) {
            $imageName = time().$request->file('image')->getClientOriginalName();

            $request->file('image')->move(public_path('images'), $imageName);
        
        }           
        $datetime=date('Y-m-d h:i:s');
        $photo_gallery = new PhotoGallery([            
            'title' => $request->get('title'),
            'image' => $imageName,
            'description' => $request->get('description'),                      
            'createdon' => $datetime            
        ]);
        //dd($asset);
        $photo_gallery->save();
        //return back()->withStatus(__('Vendor Master Added Succesfully'));
        return redirect()->to('/photo_gallery_view');
    }

    public function update(Request $request)
    { 
        $id = $request->input('id');

        $data = PhotoGallery::find($id);
                        
        if ($request->hasFile('image')) {

            $imageName = '';  
            $image_path = "images/".$data->image; 
            if (file_exists($image_path)) {

                @unlink($image_path);
            }
            $imageName = time().$request->file('image')->getClientOriginalName();

            $request->file('image')->move(public_path('images'), $imageName);  
            $update['image']=$imageName; 
            PhotoGallery::where('ID',$id)->update($update);         
        }        
        $update['title']=$request->get('title');                     
        $update['description']=$request->get('description');         
        PhotoGallery::where('ID',$id)->update($update);
                                                     
        return redirect()->to('/photo_gallery_view');
    }

    public function delete(Request $request)
    {
        //dd($request->all());
        $id=$request->get('id');
        $data = PhotoGallery::find($id);  
        $image_path = "images/".$data->image; 
        if (file_exists($image_path)) 
        {
            @unlink($image_path);
        }
        PhotoGallery::where('ID', $id)->delete();        
        return redirect()->to('/photo_gallery_view');
    }
}
