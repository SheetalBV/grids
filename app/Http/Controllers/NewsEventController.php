<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\NewsEvents;
class NewsEventController extends Controller
{
    public function index()
    {
        $news_events = NewsEvents::orderBy('ID', 'DESC')->get();    
        return view('news_events.view',compact('news_events'));
    }

    public function add()
    {
        return view('news_events.add');
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');
        $news_events = NewsEvents::where('ID',$id)->first(); 

        return view('news_events.edit',compact('id','news_events'));
    }

    public function store(Request $request)
    {  
        //dd($request->all());

        $imageName = '';              
        if ($request->hasFile('image')) {
            $imageName = time().$request->file('image')->getClientOriginalName();

            $request->file('image')->move(public_path('images'), $imageName);
        
        }           
        $datetime=date('Y-m-d h:i:s');
        $slider = new NewsEvents([            
            'title' => $request->get('title'),
            'image' => $imageName,
            'description' => $request->get('description'),
            'display_image' => $request->get('imageDisplay'),            
            'createdon' => $datetime            
        ]);
        //dd($asset);
        $slider->save();
        //return back()->withStatus(__('Vendor Master Added Succesfully'));
        return redirect()->to('/news_events_view');
    }

    public function update(Request $request)
    { 
        $id = $request->input('id');

        $data = NewsEvents::find($id);
                        
        if ($request->hasFile('image')) {

            $imageName = '';  
            $image_path = "images/".$data->image; 
            if (file_exists($image_path)) {

                @unlink($image_path);
            }
            $imageName = time().$request->file('image')->getClientOriginalName();

            $request->file('image')->move(public_path('images'), $imageName);  
            $update['image']=$imageName; 
            NewsEvents::where('ID',$id)->update($update);         
        }        
        $update['title']=$request->get('title');                     
        $update['description']=$request->get('description'); 
        $update['display_image']=$request->get('imageDisplay');
        NewsEvents::where('ID',$id)->update($update);
                                                     
        return redirect()->to('/news_events_view');
    }

    public function delete(Request $request)
    {
        //dd($request->all());
        $id=$request->get('id');
        $data = NewsEvents::find($id);  
        $image_path = "images/".$data->image; 
        if (file_exists($image_path)) 
        {
            @unlink($image_path);
        }
        NewsEvents::where('ID', $id)->delete();        
        return redirect()->to('/news_events_view');
    }
}
