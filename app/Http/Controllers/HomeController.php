<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $sliders = Slider::orderBy('ID', 'DESC')->get();    
        return view('slider.view',compact('sliders'));        
    }

    public function dashboard()
    {
        $sliders = Slider::orderBy('ID', 'DESC')->get();    
        return view('slider.view',compact('sliders'));
        // $slider_count = Slider::all()->count();
        // $total_talent = TalentCategory::all()->count();
        // $total_recent_work = RecentWork::all()->count();        
        // return view('recent_work.view', compact('slider_count','total_talent','total_recent_work'));        
        // $talent_categories = DB::table('talent_categories')->get()->keyBy('ID');
        // $recent_works = RecentWork::orderBy('ID', 'DESC')->get();
        // return view('recent_work.view',compact('recent_works','talent_categories'));
    }    
}
