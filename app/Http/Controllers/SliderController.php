<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slider;
use DB;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::orderBy('ID', 'DESC')->get();    
        return view('slider.view',compact('sliders'));
    }

    public function add()
    {
        return view('slider.add');
    }

    public function edit(Request $request)
    {
        $id = $request->input('id');
        $sliders = Slider::where('ID',$id)->first(); 

        return view('slider.edit',compact('id','sliders'));
    }

    public function store(Request $request)
    {  
        //dd($request->all());

        $imageName = '';              
        if ($request->hasFile('image')) {
            $imageName = time().$request->file('image')->getClientOriginalName();

            $request->file('image')->move(public_path('images'), $imageName);
        
        }           
        $datetime=date('Y-m-d h:i:s');
        $slider = new Slider([            
            'image' => $imageName,
            'message' => $request->get('message'),
            'createdon' => $datetime            
        ]);
        //dd($asset);
        $slider->save();
        //return back()->withStatus(__('Vendor Master Added Succesfully'));
        return redirect()->to('/slider_view');
    }

    public function update(Request $request)
    { 
        $id = $request->input('id');

        $data = Slider::find($id);
                        
        if ($request->hasFile('image')) {

            $imageName = '';  
            $image_path = "images/".$data->image; 
            if (file_exists($image_path)) {

                @unlink($image_path);
            }
            $imageName = time().$request->file('image')->getClientOriginalName();

            $request->file('image')->move(public_path('images'), $imageName);  
            $update['image']=$imageName; 
            Slider::where('ID',$id)->update($update);         
        }        
                            
        $update['message']=$request->get('message'); 
        Slider::where('ID',$id)->update($update);
          
                                           
        return redirect()->to('/slider_view');
    }

    public function delete(Request $request)
    {
        //dd($request->all());
        $id=$request->get('id');
        $data = Slider::find($id);  
        $image_path = "images/".$data->image; 
        if (file_exists($image_path)) 
        {
            @unlink($image_path);
        }
        Slider::where('ID', $id)->delete();
        //DB::delete('delete from slider where id = ?',[$id]);
        return redirect()->to('/slider_view');
    }
}
