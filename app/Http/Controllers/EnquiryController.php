<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Enquiry;
use DB;

class EnquiryController extends Controller
{

    public function index()
    {
        $enquiries = Enquiry::orderBy('ID', 'DESC')->get();    
        return view('enquiry.view',compact('enquiries'));
    }

    public function store(Request $request)
    {  
        //dd($request->all());
         
        $datetime=date('Y-m-d h:i:s');
        $Enquiry = new Enquiry([            
            'first_name' => $request->get('f_name'),
            'last_name' => $request->get('l_name'),
            'age' => $request->get('age'),
            'telephone' => $request->get('telephone'),
            'mobile' => $request->get('mobile'),
            'fax_no' => $request->get('fax_no'),
            'email' => $request->get('email'),
            'address1' => $request->get('address1'),
            'address2' => $request->get('address2'),
            'state' => $request->get('state'),
            'zipcode' => $request->get('zipcode'),
            'country' => $request->get('country'),
            'enquiry' => $request->get('enquiry'),
            'createdon' => $datetime            
        ]);
        //dd($asset);
        $Enquiry->save();        
        return redirect()->to('/enquiry')->with('status', 'Enquiry Form Submitted !');
    }

    public function delete(Request $request)
    {
        //dd($request->all());
        $id=$request->get('id');        
        Enquiry::where('ID', $id)->delete();        
        return redirect()->to('/view_enquiry');
    }
}
