<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquiries', function (Blueprint $table) {
            $table->bigIncrements('ID');
            $table->text('first_name')->nullable();
            $table->text('last_name')->nullable();   
            $table->integer('age')->nullable();  
            $table->bigInteger('telephone')->nullable();  
            $table->bigInteger('mobile')->nullable();   
            $table->text('fax_no')->nullable();   
            $table->text('email')->nullable();  
            $table->text('address1')->nullable();            
            $table->text('address2')->nullable();  
            $table->text('state')->nullable();  
            $table->text('zipcode')->nullable();  
            $table->text('country')->nullable();  
            $table->text('enquiry')->nullable();  
            $table->DateTime('createdon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquiries');
    }
}
