<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\NewsEvents;
use App\Models\PhotoGallery;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $sliders = Slider::orderBy('ID', 'ASC')->get();   
    return view('index', compact('sliders'));
});
Route::get('/enquiry', function () {      
    return view('enquiry');
});
Route::get('/contact', function () {      
    return view('contact');
});
Route::get('/aboutBES', function () {      
    return view('aboutBES');
});

Route::get('/about', function () {      
    return view('about');
});

Route::get('/courses', function () {      
    return view('courses');
});

Route::get('/photo-gallery', function () {    
    $photo_gallery = PhotoGallery::orderBy('ID', 'DESC')->get();    
    return view('photo-gallery')->with('photo_gallery',$photo_gallery);
});

Route::get('/news-and-event', function () {     
    $news_events = NewsEvents::orderBy('ID', 'DESC')->get();  
    return view('news-and-event')->with('news_events',$news_events);
});

Route::get('/comming-soon', function () {      
    return view('comming-soon');
});

Route::get('/anatomy', function () {      
    return view('anatomy');
});

Route::get('/physiology', function () {      
    return view('physiology');
});

Route::get('/biochemistry', function () {      
    return view('biochemistry');
});

Route::get('/prosthetics', function () {      
    return view('prosthetics');
});

Route::get('/pathology', function () {      
    return view('pathology');
});

Route::get('/conservative', function () {      
    return view('conservative');
});

Route::get('/pharmacology', function () {      
    return view('pharmacology');
});

Route::get('/oralpathology', function () {      
    return view('oralpathology');
});

Route::get('/generalmedicine', function () {      
    return view('generalmedicine');
});

Route::get('/generalsurgery', function () {      
    return view('generalsurgery');
});

Route::get('/pedodontics', function () {      
    return view('pedodontics');
});

Route::get('/radiology', function () {      
    return view('radiology');
});

Route::get('/orthodontics', function () {      
    return view('orthodontics');
});

Route::get('/periodontics', function () {      
    return view('periodontics');
});

Route::get('/communitydentistry', function () {      
    return view('communitydentistry');
});

Route::get('/oral-maxillofacial-surgery', function () {      
    return view('oral-maxillofacial-surgery');
});

Route::get('/oral-medicine', function () {      
    return view('oral-medicine');
});

//Store Enquiry
Route::post('/store_enquiry','App\Http\Controllers\EnquiryController@store');

//Route::get('/enquiry', 'App\Http\Controllers\SliderController@web_index');
// Route::get('/about_us', 'App\Http\Controllers\SliderController@about');
// Route::post('/store_contact', 'App\Http\Controllers\ContactUsController@store');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', 'App\Http\Controllers\HomeController@dashboard')->name('dashboard');
Route::group(['middleware' => 'auth'], function () {
	
Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
Route::put('profile/password', ['as' => 'profile/password', 'uses' => 'App\Http\Controllers\ProfileController@password']);

//Slider Master
Route::any('/slider_view','App\Http\Controllers\SliderController@index');
Route::any('/slider_add','App\Http\Controllers\SliderController@add');
Route::any('/slider_edit','App\Http\Controllers\SliderController@edit');
Route::any('/slider_store','App\Http\Controllers\SliderController@store');
Route::any('/slider_update','App\Http\Controllers\SliderController@update');
Route::any('/slider_delete','App\Http\Controllers\SliderController@delete');

//News & Events
Route::any('/news_events_view','App\Http\Controllers\NewsEventController@index');
Route::any('/news_events_add','App\Http\Controllers\NewsEventController@add');
Route::any('/news_events_edit','App\Http\Controllers\NewsEventController@edit');
Route::any('/news_events_store','App\Http\Controllers\NewsEventController@store');
Route::any('/news_events_update','App\Http\Controllers\NewsEventController@update');
Route::any('/news_events_delete','App\Http\Controllers\NewsEventController@delete');


//Photo Gallery
Route::any('/photo_gallery_view','App\Http\Controllers\PhotoGalleryController@index');
Route::any('/photo_gallery_add','App\Http\Controllers\PhotoGalleryController@add');
Route::any('/photo_gallery_edit','App\Http\Controllers\PhotoGalleryController@edit');
Route::any('/photo_gallery_store','App\Http\Controllers\PhotoGalleryController@store');
Route::any('/photo_gallery_update','App\Http\Controllers\PhotoGalleryController@update');
Route::any('/photo_gallery_delete','App\Http\Controllers\PhotoGalleryController@delete');

//Enquiries
Route::any('/view_enquiry','App\Http\Controllers\EnquiryController@index');
Route::any('/enquiry_delete','App\Http\Controllers\EnquiryController@delete');

Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});
